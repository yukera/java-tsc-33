package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.endpoint.Role;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    @Override
    public List<Role> roles() {
        @NotNull final List<Role> roles = new ArrayList<>();
        roles.add(Role.ADMIN);
        return roles;
    }

}
