package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "start project by index";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getProjectEndpoint().startProjectByIndex(serviceLocator.getSession(), index);
    }

}
