package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.api.entity.IHasNameMethod;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.endpoint.Session;
import com.tsc.jarinchekhina.tm.endpoint.Role;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractCommand implements IHasNameMethod {

    @NotNull
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void execute();

    public boolean secure() {
        @NotNull final Session session = serviceLocator.getSession();
        return serviceLocator.getUserEndpoint().checkRoles(session, roles());
    }

    @Nullable
    public List<Role> roles() {
        return null;
    }

}
