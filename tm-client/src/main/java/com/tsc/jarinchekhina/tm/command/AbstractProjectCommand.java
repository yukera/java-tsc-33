package com.tsc.jarinchekhina.tm.command;

import com.tsc.jarinchekhina.tm.endpoint.Role;
import com.tsc.jarinchekhina.tm.endpoint.Project;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public void print(@NotNull final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().value());
    }

    @NotNull
    @Override
    public List<Role> roles() {
        @NotNull final List<Role> roles = new ArrayList<>();
        roles.add(Role.USER);
        return roles;
    }

}
