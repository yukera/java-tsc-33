package com.tsc.jarinchekhina.tm.api.service;

public interface IDataService {

    void saveBackup();

    void loadBackup();

    void saveBinaryData();

    void loadBinaryData();

    void saveBase64Data();

    void loadBase64Data();

    void saveFasterXmlData(boolean isJson);

    void loadFasterXmlData(boolean isJson);

    void saveJaxbData(boolean isJson);

    void loadJaxbData(boolean isJson);

}
