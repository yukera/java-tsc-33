package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ITaskEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        serviceLocator.getTaskService().clear(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().findAll(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public Task createTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().create(userId, name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task createTaskWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().create(userId, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findById(userId, id);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findByIndex(userId, index);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findByName(userId, name);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().removeById(userId, id);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().removeByIndex(userId, index);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().removeByName(userId, name);
        return task.orElse(null);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().startTaskById(userId, id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().startTaskByIndex(userId, index);
    }

    @NotNull
    @Override
    @WebMethod
    public Task startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().startTaskByName(userId, name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().finishTaskById(userId, id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().finishTaskByIndex(userId, index);
    }

    @NotNull
    @Override
    @WebMethod
    public Task finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().finishTaskByName(userId, name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().changeTaskStatusById(userId, id, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Task changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().changeTaskStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Task changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getTaskService().changeTaskStatusByName(userId, name, status);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Task bindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectTaskService().bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @Override
    @WebMethod
    public Task unbindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getProjectTaskService().unbindTaskByProjectId(userId, taskId);
    }

}
