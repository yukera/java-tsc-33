package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "config.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "PASSWORD_SECRET";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "PASSWORD_ITERATION";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String SESSION_SECRET_KEY = "SESSION_SECRET";

    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";

    @NotNull
    private static final String SESSION_ITERATION_KEY = "SESSION_ITERATION";

    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "APPLICATION_VERSION";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_DEVELOPER_KEY = "APPLICATION_DEVELOPER";

    @NotNull
    private static final String APPLICATION_DEVELOPER_DEFAULT = "Unknown";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_KEY = "APPLICATION_DEVELOPER_EMAIL";

    @NotNull
    private static final String APPLICATION_DEVELOPER_EMAIL_DEFAULT = "Unknown";

    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "BACKUP_INTERVAL";

    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "5000";

    @NotNull
    private static final String FILE_BACKUP_KEY = "FILE_BACKUP_NAME";

    @NotNull
    private static final String FILE_BACKUP_DEFAULT = "./data-backup.xml";

    @NotNull
    private static final String FILE_BINARY_KEY = "FILE_BINARY_NAME";

    @NotNull
    private static final String FILE_BINARY_DEFAULT = "./data.bin";

    @NotNull
    private static final String FILE_BASE64_KEY = "FILE_BASE64_NAME";

    @NotNull
    private static final String FILE_BASE64_DEFAULT = "./data.base64";

    @NotNull
    private static final String FILE_FASTERXML_JSON_KEY = "FILE_FASTERXML_JSON_NAME";

    @NotNull
    private static final String FILE_FASTERXML_JSON_DEFAULT = "./data-fasterxml.json";

    @NotNull
    private static final String FILE_FASTERXML_XML_KEY = "FILE_FASTERXML_XML_NAME";

    @NotNull
    private static final String FILE_FASTERXML_XML_DEFAULT = "./data-fasterxml.xml";

    @NotNull
    private static final String FILE_JAXB_JSON_KEY = "FILE_JAXB_JSON_NAME";

    @NotNull
    private static final String FILE_JAXB_JSON_DEFAULT = "./data-jaxb.json";

    @NotNull
    private static final String FILE_JAXB_XML_KEY = "FILE_JAXB_XML_NAME";

    @NotNull
    private static final String FILE_JAXB_XML_DEFAULT = "./data-jaxb.xml";

    @NotNull
    private static final String JAXB_SYSTEM_PROP_KEY = "javax.xml.bind.context.factory";

    @NotNull
    private static final String JAXB_SYSTEM_PROP_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    private static final String JAXB_MEDIA_TYPE = "application/json";

    @NotNull
    private static final String SERVER_PORT_KEY = "SERVER_PORT";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SERVER_HOST_KEY = "SERVER_HOST";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String EMPTY_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getValue(@Nullable final String key, @Nullable final String defaultValue) {
        if (DataUtil.isEmpty(key)) return EMPTY_DEFAULT;
        if (defaultValue == null) return EMPTY_DEFAULT;
        if (System.getProperties().containsKey(key))
            return System.getProperty(key);
        if (System.getenv().containsKey(key))
            return System.getenv(key);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getDeveloper() {
        return properties.getProperty(APPLICATION_DEVELOPER_KEY, APPLICATION_DEVELOPER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return properties.getProperty(APPLICATION_DEVELOPER_EMAIL_KEY, APPLICATION_DEVELOPER_EMAIL_DEFAULT);
    }


    @NotNull
    @Override
    public String getVersion() {
        return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull String value = getValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSecret() {
        return getValue(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionIteration() {
        @NotNull String value = getValue(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public Long getBackupInterval() {
        @NotNull String value = getValue(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT);
        return Long.parseLong(value);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull String value = getValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getFileBackupName() {
        return getValue(FILE_BACKUP_KEY, FILE_BACKUP_DEFAULT);
    }

    @NotNull
    @Override
    public String getFileBinaryName() {
        return getValue(FILE_BINARY_KEY, FILE_BINARY_DEFAULT);
    }

    @NotNull
    @Override
    public String getFileBase64Name() {
        return getValue(FILE_BASE64_KEY, FILE_BASE64_DEFAULT);
    }

    @NotNull
    @Override
    public String getFileFasterXmlJsonName() {
        return getValue(FILE_FASTERXML_JSON_KEY, FILE_FASTERXML_JSON_DEFAULT);
    }

    @NotNull
    @Override
    public String getFileFasterXmlXmlName() {
        return getValue(FILE_FASTERXML_XML_KEY, FILE_FASTERXML_XML_DEFAULT);
    }

    @NotNull
    @Override
    public String getFileJaxbJsonName() {
        return getValue(FILE_JAXB_JSON_KEY, FILE_JAXB_JSON_DEFAULT);
    }

    @NotNull
    @Override
    public String getFileJaxbXmlName() {
        return getValue(FILE_JAXB_XML_KEY, FILE_JAXB_XML_DEFAULT);
    }

    @NotNull
    @Override
    public String getJaxbSystemPropKey() {
        return JAXB_SYSTEM_PROP_KEY;
    }

    @NotNull
    @Override
    public String getJaxbSystemPropValue() {
        return JAXB_SYSTEM_PROP_VALUE;
    }

    @NotNull
    @Override
    public String getJaxbMediaType() {
        return JAXB_MEDIA_TYPE;
    }

}
