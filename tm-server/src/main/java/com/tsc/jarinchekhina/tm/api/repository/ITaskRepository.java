package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Task;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IRepository<Task> {

    void clear(@NotNull String userId);

    void removeAllByProjectId(@NotNull String projectId);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId, @NotNull Comparator<Task> comparator);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task add(@NotNull String userId, @NotNull Task task);

    @NotNull
    Optional<Task> findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<Task> findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<Task> findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Optional<Task> remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    Optional<Task> removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<Task> removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<Task> removeByName(@NotNull String userId, @NotNull String name);

}
