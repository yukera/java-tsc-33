package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.other.ISaltSetting;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getValue(@Nullable String key, @Nullable String defaultValue);

    @NotNull
    String getVersion();

    @NotNull
    String getDeveloper();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    Long getBackupInterval();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getFileBackupName();

    @NotNull
    String getFileBinaryName();

    @NotNull
    String getFileBase64Name();

    @NotNull
    String getFileFasterXmlJsonName();

    @NotNull
    String getFileFasterXmlXmlName();

    @NotNull
    String getFileJaxbJsonName();

    @NotNull
    String getFileJaxbXmlName();

    @NotNull
    String getJaxbSystemPropKey();

    @NotNull
    String getJaxbSystemPropValue();

    @NotNull
    String getJaxbMediaType();

}
