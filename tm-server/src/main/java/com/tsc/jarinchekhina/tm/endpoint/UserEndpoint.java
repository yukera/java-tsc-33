package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IUserEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public User createUser(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        return serviceLocator.getUserService().create(login, password);
    }

    @NotNull
    @Override
    @WebMethod
    public User createUserWithEmail(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) {
        return serviceLocator.getUserService().create(login, password, email);
    }

    @NotNull
    @Override
    @WebMethod
    public User createUserWithRole(
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable final Role role
    ) {
        return serviceLocator.getUserService().create(login, password, role);
    }

    @NotNull
    @Override
    @WebMethod
    public User setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    @WebMethod
    public User updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    @WebMethod
    public boolean checkRoles(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "roles", partName = "roles") @Nullable final List<Role> roles
    ) {
        serviceLocator.getSessionService().validate(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        return serviceLocator.getUserService().checkRoles(userId, roles);
    }

}
