package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionEndpoint extends IEndpoint<Session> {

    @Nullable
    Session openSession(@Nullable String login, @Nullable String password);

    @NotNull
    User getUser(@Nullable Session session);

    void closeSession(@Nullable Session session);

}
