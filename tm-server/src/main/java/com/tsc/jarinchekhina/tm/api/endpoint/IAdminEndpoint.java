package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.entity.Session;
import org.jetbrains.annotations.Nullable;

public interface IAdminEndpoint {

    void saveBackup(@Nullable Session session);

    void loadBackup(@Nullable Session session);

    void saveBinaryData(@Nullable Session session);

    void loadBinaryData(@Nullable Session session);

    void saveBase64Data(@Nullable Session session);

    void loadBase64Data(@Nullable Session session);

    void saveFasterXmlData(@Nullable Session session, boolean isJson);

    void loadFasterXmlData(@Nullable Session session, boolean isJson);

    void saveJaxbData(@Nullable Session session, boolean isJson);

    void loadJaxbData(@Nullable Session session, boolean isJson);

}
