package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ISessionRepository;
import com.tsc.jarinchekhina.tm.entity.Session;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private Predicate<Session> predicateByUser(@NotNull final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @Override
    public @NotNull List<Session> findByUserId(@NotNull final String userId) {
        return entities.stream()
                .filter(predicateByUser(userId))
                .collect(Collectors.toList());
    }

}
