package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.User;
import org.jetbrains.annotations.Nullable;

public interface IAdminUserEndpoint extends IEndpoint<User> {

    @Nullable
    User findByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User removeByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User lockByLogin(@Nullable Session session, @Nullable String login);

    @Nullable
    User unlockByLogin(@Nullable Session session, @Nullable String login);

}
