package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.entity.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E add(@NotNull E entity);

    void addAll(@NotNull Collection<E> collection);

    @NotNull
    Optional<E> findById(@NotNull String id);

    void clear();

    @NotNull
    Optional<E> removeById(@NotNull String id);

    @NotNull
    Optional<E> remove(@NotNull E entity);

}
