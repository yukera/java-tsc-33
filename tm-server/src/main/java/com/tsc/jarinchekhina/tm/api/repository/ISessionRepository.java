package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Session;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    List<Session> findByUserId(@NotNull String userId);

}
