package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Project;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IProjectRepository extends IRepository<Project> {

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId, @NotNull Comparator<Project> comparator);

    @NotNull
    Project add(@NotNull String userId, @NotNull Project project);

    @NotNull
    Optional<Project> findById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<Project> findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<Project> findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Optional<Project> remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    Optional<Project> removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    Optional<Project> removeByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Optional<Project> removeByName(@NotNull String userId, @NotNull String name);

}
